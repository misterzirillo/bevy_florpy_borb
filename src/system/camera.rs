use bevy::prelude::*;

/// The width and height of the world in world-units.
#[derive(Resource, Debug)]
pub struct WorldDimensions {
    pub width: f32,
    pub height: f32,
}

pub fn compute_world_dimensions(
    // query to get camera transform
    q_camera: Query<(&Camera, &GlobalTransform)>,
    existing_edges: Option<Res<WorldDimensions>>,
    mut commands: Commands,
) {
    // get the camera info and transform
    // assuming there is exactly one main camera entity, so query::single() is OK
    let (camera, camera_transform) = q_camera.single();

    // check if the cursor is inside the window and get its position
    if existing_edges.is_none() {
        // matrix for undoing the projection and camera transform
        let ndc_to_world = camera_transform.compute_matrix() * camera.projection_matrix().inverse();

        // use it to convert ndc to world-space coordinates
        let coords = ndc_to_world.project_point3(Vec3::ONE);
        let world_dimensions = WorldDimensions {
            width: coords.x * 2.,
            height: coords.y * 2.,
        };

        info!("world dimensions {:?}", world_dimensions);
        commands.insert_resource(world_dimensions);
    }
}



