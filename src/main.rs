mod system;

use bevy::prelude::*;
use bevy::utils::HashSet;
use bevy_rapier2d::prelude::*;

#[cfg(debug_assertions)]
use bevy_inspector_egui::WorldInspectorPlugin;

use std::f32::consts::PI;
use std::time::Duration;

use crate::system::*;

fn main() {
    let mut app = App::new();

    app.add_plugins(DefaultPlugins)
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(1000.0))
        .insert_resource(ClearColor(*Color::hex("7aa1fa").unwrap().set_a(0.5)))
        .add_startup_system(setup_graphics)
        .add_startup_system(setup_bird)
        .add_system(user_input)
        .add_system(camera::compute_world_dimensions)
        .add_system(pipe_timer)
        .add_system(move_pipes)
        .add_system(despawn_pipes)
        .add_system(detect_collision);

    #[cfg(debug_assertions)]
    app.add_plugin(WorldInspectorPlugin::new())
        .add_plugin(RapierDebugRenderPlugin::default());

    app.run();
}

fn setup_graphics(mut commands: Commands) {
    // Add a camera so we can see the debug-render.
    let camera = Camera2dBundle {
        projection: OrthographicProjection {
            scale: 2.,
            ..default()
        },
        ..default()
    };
    commands.spawn(camera);
}

#[derive(Component)]
struct Birb;

fn setup_bird(mut commands: Commands, loader: Res<AssetServer>) {
    /* Create the bouncing ball. */
    commands
        .spawn((
            GravityScale(6.),
            Birb,
            SpriteBundle {
                texture: loader.load("sprites/unitytut-birdbody_sm.png"),
                sprite: Sprite {
                    anchor: bevy::sprite::Anchor::Custom(Vec2::new(-0.02, -0.1)),
                    ..default()
                },
                transform: Transform::from_xyz(0., 200.0, 1.0),
                ..default()
            },
            RigidBody::Dynamic,
            Collider::ball(90.0),
            ColliderMassProperties::Density(1.0),
            Restitution::coefficient(0.5),
            ExternalImpulse::default(),
        ))
        .with_children(|parent| {
            let wing_scale = Vec3::new(1.5, 1.5, 1.);

            // left wing
            parent.spawn((SpriteBundle {
                texture: loader.load("sprites/unitytut-birdwingdown_sm.png"),
                transform: Transform {
                    translation: Vec3::new(-100., -30., -1.),
                    scale: wing_scale,
                    ..default()
                },
                ..default()
            },));

            // right wing
            parent.spawn((SpriteBundle {
                texture: loader.load("sprites/unitytut-birdwingdown_sm.png"),
                transform: Transform {
                    translation: Vec3::new(100., -30., -1.),
                    scale: wing_scale,
                    rotation: Quat::from_axis_angle(Vec3::new(0., 1., 0.), PI),
                },
                ..default()
            },));
        });

    // pipe config
    commands.insert_resource(PipeSpawnConfig {
        timer: Timer::new(Duration::from_secs(3), TimerMode::Repeating),
    });

    // pipe assets
    let pipe_texture: Handle<Image> = loader.load("sprites/unitytut-pipe.png");
    commands.insert_resource(PipeTexture(pipe_texture));

    // background
}

fn user_input(keeb: Res<Input<KeyCode>>, mut q_impulse: Query<&mut ExternalImpulse, With<Birb>>) {
    if keeb.just_pressed(KeyCode::Space) {
        for mut i in q_impulse.iter_mut() {
            i.impulse = Vec2::new(0., 10.);
        }
    }
}

#[derive(Resource)]
struct PipeSpawnConfig {
    timer: Timer,
}

#[derive(Resource)]
struct PipeTexture(Handle<Image>);

fn pipe_timer(
    commands: Commands,
    time: Res<Time>,
    world_dim: Option<Res<camera::WorldDimensions>>,
    pipe_texture: Res<PipeTexture>,
    mut pipe_spawn_config: ResMut<PipeSpawnConfig>,
) {
    pipe_spawn_config.timer.tick(time.delta());

    if pipe_spawn_config.timer.finished() && world_dim.is_some() {
        let world_x = world_dim.unwrap().width / 2.;
        spawn_pipe_pair(commands, &pipe_texture.0, world_x);
    }
}

#[derive(Component)]
struct PipePair;

#[derive(Component)]
struct Pipe;

const PIPE_HALF_WIDTH: f32 = 50.;
const PIPE_AREA: Vec2 = Vec2::new(PIPE_HALF_WIDTH * 2., 1000.);

fn spawn_pipe_pair(mut commands: Commands, pipe_texture: &Handle<Image>, spawn_x: f32) {
    let pair_y = (rand::random::<f32>() - 0.5) * 300.;

    commands
        .spawn((
            SpatialBundle {
                transform: Transform::from_xyz(spawn_x + PIPE_HALF_WIDTH, pair_y, 1.),
                ..default()
            },
            PipePair,
        ))
        .with_children(|parent| {
            // upper pipe
            parent.spawn((
                SpriteBundle {
                    transform: Transform {
                        translation: Vec3::new(0., 800., 1.),
                        ..default()
                    },
                    texture: pipe_texture.clone(),
                    sprite: Sprite {
                        custom_size: Some(PIPE_AREA),
                        ..default()
                    },
                    ..default()
                },
                Pipe,
                RigidBody::KinematicPositionBased,
                Collider::cuboid(PIPE_HALF_WIDTH, PIPE_AREA.y / 2.),
                ActiveEvents::COLLISION_EVENTS,
            ));

            // lower pipe
            parent.spawn((
                SpriteBundle {
                    transform: Transform {
                        translation: Vec3::new(0., -800., 1.),
                        ..default()
                    },
                    texture: pipe_texture.clone(),
                    sprite: Sprite {
                        custom_size: Some(PIPE_AREA),
                        flip_y: true,
                        ..default()
                    },
                    ..default()
                },
                Pipe,
                RigidBody::KinematicPositionBased,
                Collider::cuboid(PIPE_HALF_WIDTH, PIPE_AREA.y / 2.),
                ActiveEvents::COLLISION_EVENTS,
            ));
        });
}

fn move_pipes(mut q_pipes: Query<&mut Transform, With<PipePair>>) {
    for mut t in q_pipes.iter_mut() {
        t.translation.x -= 5.;
    }
}

fn despawn_pipes(
    mut q_pipes: Query<(&Transform, Entity), With<PipePair>>,
    mut commands: Commands,
    edges: Option<Res<camera::WorldDimensions>>,
) {
    // when the world dimensions are available despawn offscreen pipes
    if let Some(world_dimensions) = edges {
        let max_distance = world_dimensions.width / -2. - PIPE_HALF_WIDTH;
        for (_, e) in q_pipes
            .iter_mut()
            .filter(|(t, _)| t.translation.x < max_distance)
        {
            commands.entity(e).despawn_recursive();
        }
    }
}

fn detect_collision(
    mut collision_events: EventReader<CollisionEvent>,
    q_birb_and_pipes: Query<Entity, Or<(With<Pipe>, With<Birb>)>>,
    mut exit: EventWriter<bevy::app::AppExit>,
) {
    let entities = q_birb_and_pipes.iter().collect::<HashSet<Entity>>();

    for evt in collision_events.iter() {
        if let CollisionEvent::Started(e1, e2, _) = evt {
            if entities.contains(e1) && entities.contains(e2) {
                // TODO end game
                info!("Game over.");
                exit.send(bevy::app::AppExit);
            }
        }
    }
}
